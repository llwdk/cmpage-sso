'use strict';
// +----------------------------------------------------------------------
// | CmPage-sso [ CmPage 用户中心 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 @module api.controller
 */

/**
 * 用户相关的对外API接口,版本：v1.1
 返回码：
 200	正常
 300	业务逻辑异常
 301	服务器端系统异常
 302	平台账号不存在
 303	登录认证失败
 304	平台账号密码不正确
 305	登录ip与上次不一致
 306	token 超时
 * @class api.controller.v1
 */
import Base from './base.js';
import moment from 'moment';

export default class extends Base {
    /**
    * 子系统用户登录，调用：/api/user1/sso_login?login_name=xxx&login_pwd=xxxx&ip=xxx&sub_system=xxx&memo=xxx
    * 其中 login_pwd 需要MD5加密
    * @method  ssoLogin
    * @return {json} 登录状态对象，{statusCode:xxx, message:xxx, data:{guid:xxx, token:xxx }}
    */
    async ssoLoginAction(){
        let parms = this.get();
        parms.ip = this.get('ip') || this.ip();
        let user = await cmpage.model('user/ssuser').getUserByLoginName(parms.login_name);
        if(think.isEmpty(user)) return {statusCode:302, message:'用户不存在！', data:{guid:''}, };
        if(user.c_login_pwd != parms.login_pwd) return {statusCode:304, message:'密码错误！', data:{guid:''}, };

        //增加登录日志
        user.ip = parms.ip;
        user.sub_system = parms.sub_system || '';
        user.c_memo = parms.memo ||'';
        let login = await cmpage.model('user/sslogin').addLogin(user);

        let ret = {statusCode:200, message:'', data:{guid:user.c_guid, token:login.c_token }};
        debug(ret,'api.user101.ssoLoginAction - ret');
        return this.json(ret);
    }

    /**
    * 子系统用token值校验，调用：/api/user1/login_verify?token=xxxx&ip=xxx&subSystem=xxx
    * 其中 loginPwd 需要MD5加密
    * @method  loginVerify
    * @return {json} 登录状态对象，{statusCode:xxx, message:xxx, data:{guid:xxx, token:xxx }}
    */
    async loginVerifyAction(){
        let parms = this.get();
        parms.ip = this.get('ip') || this.ip();
        let loginModel = cmpage.model('user/sslogin');
        let login = await loginModel.getLoginByToken(parms.token);

        if(think.isEmpty(login)) return {statusCode:303, message:'用户还没有登录！',data:{guid:''} };
        //token超时时间暂定为 两小时
        if(moment().isAfter(moment(login.c_time).add(2,'hours')))   return {statusCode:306, message:'用户登录已超时！',data:{guid:''} };

        return this.json({statusCode:200, message:'', data:{guid:login.c_guid, token:login.c_token }});
    }

    /**
    * 增加用户信息， 调用：/api/user1/add_user?login_name=xxx&phone=xxx&name=xxx&login_pwd=xxx
    * @method  addUser
    * @return {json} 返回状态 {statusCode:xxx, message:xxx, data:user}
    */
    async addUserAction(){
        let parms = this.get();
        let user= {c_login_name:parms.login_name, c_name:parms.name, c_phone:parms.phone, c_login_pwd:parms.login_pwd};
        let ret = await cmpage.model('user/ssuser').addUser( user);
        return this.json(ret);
    }

    /**
    * 修改用户信息， 调用：/api/user1/update_user?guid=xxx&login_name=xxx&name=xxx&phone=xxx
    * @method  updateUser
    * @return {json} 返回修改状态 {statusCode:xxx, message:xxx, data:user}
    */
    async updateUserAction(){
        let parms = this.get();
        let user= {c_login_name:parms.login_name, c_name:parms.name, c_phone:parms.phone};
        let ret = await cmpage.model('user/ssuser').updateUserByGuid(parms.guid, user);
        ret.data = user;
        return this.json(ret);
    }

    /**
    * 更新用户密码， 调用：/api/user1/update_user_pwd?guid=xxx&old_pwd=xxx&new_pwd=xxx
    * 其中 login_pwd,old_pwd 需要MD5加密
    * @method  updateUserPwd
    * @return {json} 返回修改状态 {statusCode:xxx, message:xxx, data:{guid:xxx, old_pwd:xxx, new_pwd:xxx}}}
    */
    async updateUserPwdAction(){
        let parms = this.get();
        let ret = await cmpage.model('user/ssuser').updateUserPwd(parms.guid, parms.old_pwd, parms.new_pwd);
        ret.data = parms;
        return this.json(ret);
    }

    /**
    * 初始化用户密码， 调用：/api/user1/init_user_pwd?guid=xxx
    * @method  initUserPwd
    * @return {json} 返回修改状态 {statusCode:xxx, message:xxx, data:{guid:xxx, login_pwd:xxx}}}
    */
    async initUserPwdAction(){
        let parms = this.get();
        let ret = await cmpage.model('user/ssuser').initUserPwd(parms.guid);
        return this.json(ret);
    }

    /**
    * 发送手机验证码， 调用：/api/user1/send_captcha?phone=xxx
    * @method  sendCaptcha
    * @return {json} 返回手机号及验证码 {statusCode:xxx, message:xxx, data:{phone:xxx, catpcha:xxx}}}
    */
    async sendCaptchaAction(){
        let phone = this.get('phone');
        let catpcha = cmpage.getRandomNum(1000,9999);
        //调用接口发送手机验证码

        let ret = {statusCode:200, message:'', data:{phone:phone, catpcha:catpcha}};
        return this.json(ret);
    }


}
