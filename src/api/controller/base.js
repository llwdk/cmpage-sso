'use strict';
// +----------------------------------------------------------------------
// | CmPage-sso [ CmPage 用户中心 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------

/**
 CmPage 用户中心的对外API接口的controller模块，实现了对外的API接口，

返回码：
200	正常
300	业务逻辑异常
301	服务器端系统异常
302	平台账号不存在
303	登录认证失败
304	平台账号密码不正确
305	登录ip与上次不一致
306	ip无效

 @module api.controller
 */

export default class extends think.controller.base {
    /**
     本模块的所有action执行前的检查项
     @method  __before
     @return {promise} 当前用户未登录时，返回错误信息或者引导到登录页面
     */
  async __before(){
    //部分 action 下不检查,
    let blankActions = ["sso_login"];
    //debug(this.http.action,'base.__before - actionName');
    if(blankActions.indexOf(this.http.action) == -1){
        let ip = this.ip()+',';
        //debug(ip,'api.base.C.__before - ip');
        let white_ips = this.config('white_ips')+',';
        //debug(white_ips,'api.base.C.__before - white_ips');

        if(white_ips.indexOf(ip) <0 && ip.indexOf('192.168.')!=0 && ip.indexOf('127.')!=0){
            return this.json({ statusCode :306, message : "无效的IP地址！" });
        }
    }
  }

}
