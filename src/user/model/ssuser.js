'use strict';
// +----------------------------------------------------------------------
// | CmPage [ 通用页面框架 ]
// +----------------------------------------------------------------------
// | Licensed under the Apache License, Version 2.0
// +----------------------------------------------------------------------
// | Author: defans <defans@sina.cn>
// +----------------------------------------------------------------------
/**
 @module admin.model
 */

/**
 * 登录用户的操作类，提供一些操作t_user,vw_user的方法
 * @class admin.model.user
 */
import CMPage from '../../cmpage/model/page.js';

export default class extends CMPage {

    /**
     * 新增的时候，初始化编辑页面的值，子类重写本方法可以定制新增页面的初始值
     * @method  pageEditInit
     * @return {object} 新增的记录对象
     */
    async pageEditInit(){
        let md =await super.pageEditInit();
        md.c_guid=think.uuid();
        md.c_login_pwd = think.md5('123456');
        md.c_status = cmpage.enumUserStatus.NORMAL;

        return md
    }
    /**
     * 用户密码修改
     * @method  updateUserPwd
     * @return {object}  修改状态
     * @param {string} guid  用户GUID
     * @param {string} old_pwd  老密码
     * @param {string} new_pwd  新密码
     */
    async updateUserPwd(guid, old_pwd, new_pwd){
        let user = this.model('t_user').where(`c_guid='${guid}'`).find();
        if(think.isEmpty(user)) return {statusCode:302, message:'账户不存在！'};
        if(user.c_login_pwd != old_pwd) return {statusCode:300, message:'用户原始密码错误！'};

        await this.model('t_user').where(`c_guid='${guid}'`).update({c_login_pwd:new_pwd});
        await think.cache("users", null);
        return {statusCode:200, message:''};
    }

    /**
     * 新增用户资料
     * @method  addUser
     * @return {object}  修改状态
     * @param {object} user  用户对象 {c_login_name:xxx, c_phone:xxx, c_name:xxx, c_login_pwd:xxx}
     */
    async addUser(user){
        user.c_guid = user.c_guid || think.uuid();
        user.c_login_pwd = user.c_login_pwd || think.md5('123456');
        user.c_name = user.c_name || user.c_login_name;
        user.c_role = 0;

        user.id = await this.model('t_user').add(user);
        await think.cache("users", null);
        return {statusCode:200, message:'', data:user};
    }

    /**
     * 初始化用户密码
     * @method  initUserPwd
     * @return {object}  修改状态
     * @param {string} guid  用户GUID
     */
    async initUserPwd(guid){
        let loginPwd = think.md5('123456');
        await this.model('t_user').where(`c_guid='${guid}'`).update({c_login_pwd:loginPwd});
        await think.cache("users", null);
        return {statusCode:200, message:'',data:{guid:guid, login_pwd:loginPwd}};
    }

    /**
     * 根据用户GUID修改用户资料
     * @method  updateUserByGuid
     * @return {object}  修改状态
     * @param {string} guid  用户GUID
     * @param {object} user  用户对象
     */
    async updateUserByGuid(guid, user){
        await this.model('t_user').where(`c_guid='${guid}'`).update(user);
        await think.cache("users", null);
        return {statusCode:200, message:'',data:user};
    }

    /**
     * 根据用户ID取用户名称，一般用于页面模块配置中的‘替换’调用: admin/user:getNameById
     * @method  getNameById
     * @return {string}  用户名称
     * @param {int} id  用户ID
     */
    async getNameById(id){
        let users =await this.getUsers();
        for(let user of users){
            if(user.id == id){
                return user.c_name;
            }
        }
        return '';
    }
    /**
     * 根据用户ID取用户对象
     * @method  getNameById
     * @return {object}  用户对象
     * @param {int} id  用户ID
     */
    async getUserById(id){
        let users =await this.getUsers();
        for(let user of users){
            if(user.id == id){
                return user;
            }
        }
        return {};
    }
    /**
     * 根据用户登录名取用户记录对象，
     * @method  getUserByLoginName
     * @return {object}  用户信息
     * @param {string} loginName  登录名
     */
    async getUserByLoginName(loginName){
        let users =await this.getUsers();
        //cmpage.debug(users);
        for(let user of users){
            if(user.c_login_name == loginName && user.c_status==1){
                return user;
            }
        }
        return {};
    }
    /**
     * 根据用户登录名和密码取用户记录对象，
     * @method  getUserByLogin
     * @return {object}  用户信息
     * @param {string} loginName  登录名
     * @param {string} loginPwd  登录密码
     * @param {bool} isMd5  是否是MD5加密过的
     */
    async getUserByLogin(loginName,loginPwd, isMd5){
        let users =await this.getUsers();
        let pwd = isMd5 ? loginPwd.toLowerCase() : think.md5(loginPwd).toLowerCase()
        //cmpage.debug(users);
        for(let user of users){
            if(user.c_login_name == loginName && user.c_login_pwd == pwd && user.c_status==1){
                return user;
            }
        }
        return {};
    }
    /**
     * 取vw_user的记录，缓存， 按名称排序
     * @method  getUsers
     * @return {Array}  vw_user记录列表
     */
    async getUsers(){
        return await think.cache("users", () => {
            //return this.model('vw_user').order('c_name').select();
            return this.query('select * from t_user order by  c_name ');
        });
    }


}
