var kafka = require('kafka-node');
//var KeyedMessage = kafka.KeyedMessage;
var client = new kafka.Client('localhost:2181');

var producer = new kafka.Producer(client, {
    requireAcks: 1
});
producer.on('ready', function() {
    // var keyedMessage = new KeyedMessage('keyed', 'a keyed message');
    producer.send([{
        topic: 'test',
        partition: 0,
        messages: 'Hello'
    }], function(err, result) {
        console.log(err || result);
    });

    //create topics
    // producer.createTopics(['t1'], function (err, data) {
    //     console.log(data);
    // });
});
